import{c as T}from"./index.0579b088.js";import{r as d,a as _,j as h,I as H}from"./app.75cef797.js";import{t as U}from"./transition.f3e48ab6.js";function Pt({align:t="right",width:e="48",contentClasses:r="py-1 bg-white",renderTrigger:s,children:o}){const[a,n]=d.exports.useState(!1),i={48:"w-48"}[e.toString()],l=(()=>t==="left"?"origin-top-left left-0":t==="right"?"origin-top-right right-0":"origin-top")();return _("div",{className:"relative",children:[h("div",{onClick:()=>n(!a),children:s()}),h("div",{className:"fixed inset-0 z-40",style:{display:a?"block":"none"},onClick:()=>n(!1)}),h(U,{show:a,enter:"transition ease-out duration-200",enterFrom:"transform opacity-0 scale-95",enterTo:"transform opacity-100 scale-100",leave:"transition ease-in duration-75",leaveFrom:"transform opacity-100 scale-100",leaveTo:"transform opacity-0 scale-95",className:"relative z-50",children:h("div",{className:T("absolute mt-2 rounded-md shadow-lg",i,l),onClick:()=>n(!1),children:h("div",{className:T("rounded-md ring-1 ring-black ring-opacity-5",r),children:o})})})]})}function Ft({as:t,href:e,children:r}){return h("div",{children:(()=>{switch(t){case"button":return h("button",{type:"submit",className:"block w-full px-4 py-2 text-sm leading-5 text-gray-700 text-left hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition",children:r});case"a":return h("a",{href:e,className:"block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition",children:r});default:return h(H,{href:e||"",className:"block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition",children:r})}})()})}let R={data:""},Y=t=>typeof window=="object"?((t?t.querySelector("#_goober"):window._goober)||Object.assign((t||document.head).appendChild(document.createElement("style")),{innerHTML:" ",id:"_goober"})).firstChild:t||R,Z=/(?:([\u0080-\uFFFF\w-%@]+) *:? *([^{;]+?);|([^;}{]*?) *{)|(}\s*)/g,q=/\/\*[^]*?\*\/|  +/g,P=/\n+/g,b=(t,e)=>{let r="",s="",o="";for(let a in t){let n=t[a];a[0]=="@"?a[1]=="i"?r=a+" "+n+";":s+=a[1]=="f"?b(n,a):a+"{"+b(n,a[1]=="k"?"":e)+"}":typeof n=="object"?s+=b(n,e?e.replace(/([^,])+/g,i=>a.replace(/(^:.*)|([^,])+/g,l=>/&/.test(l)?l.replace(/&/g,i):i?i+" "+l:l)):a):n!=null&&(a=/^--/.test(a)?a:a.replace(/[A-Z]/g,"-$&").toLowerCase(),o+=b.p?b.p(a,n):a+":"+n+";")}return r+(e&&o?e+"{"+o+"}":o)+s},y={},S=t=>{if(typeof t=="object"){let e="";for(let r in t)e+=r+S(t[r]);return e}return t},B=(t,e,r,s,o)=>{let a=S(t),n=y[a]||(y[a]=(l=>{let c=0,p=11;for(;c<l.length;)p=101*p+l.charCodeAt(c++)>>>0;return"go"+p})(a));if(!y[n]){let l=a!==t?t:(c=>{let p,f,m=[{}];for(;p=Z.exec(c.replace(q,""));)p[4]?m.shift():p[3]?(f=p[3].replace(P," ").trim(),m.unshift(m[0][f]=m[0][f]||{})):m[0][p[1]]=p[2].replace(P," ").trim();return m[0]})(t);y[n]=b(o?{["@keyframes "+n]:l}:l,r?"":"."+n)}let i=r&&y.g?y.g:null;return r&&(y.g=y[n]),((l,c,p,f)=>{f?c.data=c.data.replace(f,l):c.data.indexOf(l)===-1&&(c.data=p?l+c.data:c.data+l)})(y[n],e,s,i),n},G=(t,e,r)=>t.reduce((s,o,a)=>{let n=e[a];if(n&&n.call){let i=n(r),l=i&&i.props&&i.props.className||/^go/.test(i)&&i;n=l?"."+l:i&&typeof i=="object"?i.props?"":b(i,""):i===!1?"":i}return s+o+(n==null?"":n)},"");function O(t){let e=this||{},r=t.call?t(e.p):t;return B(r.unshift?r.raw?G(r,[].slice.call(arguments,1),e.p):r.reduce((s,o)=>Object.assign(s,o&&o.call?o(e.p):o),{}):r,Y(e.target),e.g,e.o,e.k)}let M,I,z;O.bind({g:1});let x=O.bind({k:1});function J(t,e,r,s){b.p=e,M=t,I=r,z=s}function v(t,e){let r=this||{};return function(){let s=arguments;function o(a,n){let i=Object.assign({},a),l=i.className||o.className;r.p=Object.assign({theme:I&&I()},i),r.o=/ *go\d+/.test(l),i.className=O.apply(r,s)+(l?" "+l:""),e&&(i.ref=n);let c=t;return t[0]&&(c=i.as||t,delete i.as),z&&c[0]&&z(i),M(c,i)}return e?e(o):o}}var Q=t=>typeof t=="function",C=(t,e)=>Q(t)?t(e):t,V=(()=>{let t=0;return()=>(++t).toString()})(),L=(()=>{let t;return()=>{if(t===void 0&&typeof window<"u"){let e=matchMedia("(prefers-reduced-motion: reduce)");t=!e||e.matches}return t}})(),W=20,$=new Map,X=1e3,F=t=>{if($.has(t))return;let e=setTimeout(()=>{$.delete(t),w({type:4,toastId:t})},X);$.set(t,e)},K=t=>{let e=$.get(t);e&&clearTimeout(e)},A=(t,e)=>{switch(e.type){case 0:return{...t,toasts:[e.toast,...t.toasts].slice(0,W)};case 1:return e.toast.id&&K(e.toast.id),{...t,toasts:t.toasts.map(a=>a.id===e.toast.id?{...a,...e.toast}:a)};case 2:let{toast:r}=e;return t.toasts.find(a=>a.id===r.id)?A(t,{type:1,toast:r}):A(t,{type:0,toast:r});case 3:let{toastId:s}=e;return s?F(s):t.toasts.forEach(a=>{F(a.id)}),{...t,toasts:t.toasts.map(a=>a.id===s||s===void 0?{...a,visible:!1}:a)};case 4:return e.toastId===void 0?{...t,toasts:[]}:{...t,toasts:t.toasts.filter(a=>a.id!==e.toastId)};case 5:return{...t,pausedAt:e.time};case 6:let o=e.time-(t.pausedAt||0);return{...t,pausedAt:void 0,toasts:t.toasts.map(a=>({...a,pauseDuration:a.pauseDuration+o}))}}},N=[],j={toasts:[],pausedAt:void 0},w=t=>{j=A(j,t),N.forEach(e=>{e(j)})},tt={blank:4e3,error:4e3,success:2e3,loading:1/0,custom:4e3},et=(t={})=>{let[e,r]=d.exports.useState(j);d.exports.useEffect(()=>(N.push(r),()=>{let o=N.indexOf(r);o>-1&&N.splice(o,1)}),[e]);let s=e.toasts.map(o=>{var a,n;return{...t,...t[o.type],...o,duration:o.duration||((a=t[o.type])==null?void 0:a.duration)||(t==null?void 0:t.duration)||tt[o.type],style:{...t.style,...(n=t[o.type])==null?void 0:n.style,...o.style}}});return{...e,toasts:s}},rt=(t,e="blank",r)=>({createdAt:Date.now(),visible:!0,type:e,ariaProps:{role:"status","aria-live":"polite"},message:t,pauseDuration:0,...r,id:(r==null?void 0:r.id)||V()}),k=t=>(e,r)=>{let s=rt(e,t,r);return w({type:2,toast:s}),s.id},u=(t,e)=>k("blank")(t,e);u.error=k("error");u.success=k("success");u.loading=k("loading");u.custom=k("custom");u.dismiss=t=>{w({type:3,toastId:t})};u.remove=t=>w({type:4,toastId:t});u.promise=(t,e,r)=>{let s=u.loading(e.loading,{...r,...r==null?void 0:r.loading});return t.then(o=>(u.success(C(e.success,o),{id:s,...r,...r==null?void 0:r.success}),o)).catch(o=>{u.error(C(e.error,o),{id:s,...r,...r==null?void 0:r.error})}),t};var at=(t,e)=>{w({type:1,toast:{id:t,height:e}})},ot=()=>{w({type:5,time:Date.now()})},st=t=>{let{toasts:e,pausedAt:r}=et(t);d.exports.useEffect(()=>{if(r)return;let a=Date.now(),n=e.map(i=>{if(i.duration===1/0)return;let l=(i.duration||0)+i.pauseDuration-(a-i.createdAt);if(l<0){i.visible&&u.dismiss(i.id);return}return setTimeout(()=>u.dismiss(i.id),l)});return()=>{n.forEach(i=>i&&clearTimeout(i))}},[e,r]);let s=d.exports.useCallback(()=>{r&&w({type:6,time:Date.now()})},[r]),o=d.exports.useCallback((a,n)=>{let{reverseOrder:i=!1,gutter:l=8,defaultPosition:c}=n||{},p=e.filter(g=>(g.position||c)===(a.position||c)&&g.height),f=p.findIndex(g=>g.id===a.id),m=p.filter((g,D)=>D<f&&g.visible).length;return p.filter(g=>g.visible).slice(...i?[m+1]:[0,m]).reduce((g,D)=>g+(D.height||0)+l,0)},[e]);return{toasts:e,handlers:{updateHeight:at,startPause:ot,endPause:s,calculateOffset:o}}},it=x`
from {
  transform: scale(0) rotate(45deg);
	opacity: 0;
}
to {
 transform: scale(1) rotate(45deg);
  opacity: 1;
}`,nt=x`
from {
  transform: scale(0);
  opacity: 0;
}
to {
  transform: scale(1);
  opacity: 1;
}`,lt=x`
from {
  transform: scale(0) rotate(90deg);
	opacity: 0;
}
to {
  transform: scale(1) rotate(90deg);
	opacity: 1;
}`,ct=v("div")`
  width: 20px;
  opacity: 0;
  height: 20px;
  border-radius: 10px;
  background: ${t=>t.primary||"#ff4b4b"};
  position: relative;
  transform: rotate(45deg);

  animation: ${it} 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275)
    forwards;
  animation-delay: 100ms;

  &:after,
  &:before {
    content: '';
    animation: ${nt} 0.15s ease-out forwards;
    animation-delay: 150ms;
    position: absolute;
    border-radius: 3px;
    opacity: 0;
    background: ${t=>t.secondary||"#fff"};
    bottom: 9px;
    left: 4px;
    height: 2px;
    width: 12px;
  }

  &:before {
    animation: ${lt} 0.15s ease-out forwards;
    animation-delay: 180ms;
    transform: rotate(90deg);
  }
`,dt=x`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`,pt=v("div")`
  width: 12px;
  height: 12px;
  box-sizing: border-box;
  border: 2px solid;
  border-radius: 100%;
  border-color: ${t=>t.secondary||"#e0e0e0"};
  border-right-color: ${t=>t.primary||"#616161"};
  animation: ${dt} 1s linear infinite;
`,ut=x`
from {
  transform: scale(0) rotate(45deg);
	opacity: 0;
}
to {
  transform: scale(1) rotate(45deg);
	opacity: 1;
}`,mt=x`
0% {
	height: 0;
	width: 0;
	opacity: 0;
}
40% {
  height: 0;
	width: 6px;
	opacity: 1;
}
100% {
  opacity: 1;
  height: 10px;
}`,ft=v("div")`
  width: 20px;
  opacity: 0;
  height: 20px;
  border-radius: 10px;
  background: ${t=>t.primary||"#61d345"};
  position: relative;
  transform: rotate(45deg);

  animation: ${ut} 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275)
    forwards;
  animation-delay: 100ms;
  &:after {
    content: '';
    box-sizing: border-box;
    animation: ${mt} 0.2s ease-out forwards;
    opacity: 0;
    animation-delay: 200ms;
    position: absolute;
    border-right: 2px solid;
    border-bottom: 2px solid;
    border-color: ${t=>t.secondary||"#fff"};
    bottom: 6px;
    left: 6px;
    height: 10px;
    width: 6px;
  }
`,gt=v("div")`
  position: absolute;
`,yt=v("div")`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 20px;
  min-height: 20px;
`,ht=x`
from {
  transform: scale(0.6);
  opacity: 0.4;
}
to {
  transform: scale(1);
  opacity: 1;
}`,xt=v("div")`
  position: relative;
  transform: scale(0.6);
  opacity: 0.4;
  min-width: 20px;
  animation: ${ht} 0.3s 0.12s cubic-bezier(0.175, 0.885, 0.32, 1.275)
    forwards;
`,bt=({toast:t})=>{let{icon:e,type:r,iconTheme:s}=t;return e!==void 0?typeof e=="string"?d.exports.createElement(xt,null,e):e:r==="blank"?null:d.exports.createElement(yt,null,d.exports.createElement(pt,{...s}),r!=="loading"&&d.exports.createElement(gt,null,r==="error"?d.exports.createElement(ct,{...s}):d.exports.createElement(ft,{...s})))},vt=t=>`
0% {transform: translate3d(0,${t*-200}%,0) scale(.6); opacity:.5;}
100% {transform: translate3d(0,0,0) scale(1); opacity:1;}
`,wt=t=>`
0% {transform: translate3d(0,0,-1px) scale(1); opacity:1;}
100% {transform: translate3d(0,${t*-150}%,-1px) scale(.6); opacity:0;}
`,kt="0%{opacity:0;} 100%{opacity:1;}",Et="0%{opacity:1;} 100%{opacity:0;}",$t=v("div")`
  display: flex;
  align-items: center;
  background: #fff;
  color: #363636;
  line-height: 1.3;
  will-change: transform;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.1), 0 3px 3px rgba(0, 0, 0, 0.05);
  max-width: 350px;
  pointer-events: auto;
  padding: 8px 10px;
  border-radius: 8px;
`,Nt=v("div")`
  display: flex;
  justify-content: center;
  margin: 4px 10px;
  color: inherit;
  flex: 1 1 auto;
  white-space: pre-line;
`,jt=(t,e)=>{let r=t.includes("top")?1:-1,[s,o]=L()?[kt,Et]:[vt(r),wt(r)];return{animation:e?`${x(s)} 0.35s cubic-bezier(.21,1.02,.73,1) forwards`:`${x(o)} 0.4s forwards cubic-bezier(.06,.71,.55,1)`}},Ct=d.exports.memo(({toast:t,position:e,style:r,children:s})=>{let o=t.height?jt(t.position||e||"top-center",t.visible):{opacity:0},a=d.exports.createElement(bt,{toast:t}),n=d.exports.createElement(Nt,{...t.ariaProps},C(t.message,t));return d.exports.createElement($t,{className:t.className,style:{...o,...r,...t.style}},typeof s=="function"?s({icon:a,message:n}):d.exports.createElement(d.exports.Fragment,null,a,n))});J(d.exports.createElement);var Ot=({id:t,className:e,style:r,onHeightUpdate:s,children:o})=>{let a=d.exports.useCallback(n=>{if(n){let i=()=>{let l=n.getBoundingClientRect().height;s(t,l)};i(),new MutationObserver(i).observe(n,{subtree:!0,childList:!0,characterData:!0})}},[t,s]);return d.exports.createElement("div",{ref:a,className:e,style:r},o)},Dt=(t,e)=>{let r=t.includes("top"),s=r?{top:0}:{bottom:0},o=t.includes("center")?{justifyContent:"center"}:t.includes("right")?{justifyContent:"flex-end"}:{};return{left:0,right:0,display:"flex",position:"absolute",transition:L()?void 0:"all 230ms cubic-bezier(.21,1.02,.73,1)",transform:`translateY(${e*(r?1:-1)}px)`,...s,...o}},It=O`
  z-index: 9999;
  > * {
    pointer-events: auto;
  }
`,E=16,St=({reverseOrder:t,position:e="top-center",toastOptions:r,gutter:s,children:o,containerStyle:a,containerClassName:n})=>{let{toasts:i,handlers:l}=st(r);return d.exports.createElement("div",{style:{position:"fixed",zIndex:9999,top:E,left:E,right:E,bottom:E,pointerEvents:"none",...a},className:n,onMouseEnter:l.startPause,onMouseLeave:l.endPause},i.map(c=>{let p=c.position||e,f=l.calculateOffset(c,{reverseOrder:t,gutter:s,defaultPosition:e}),m=Dt(p,f);return d.exports.createElement(Ot,{id:c.id,key:c.id,onHeightUpdate:l.updateHeight,className:c.visible?It:"",style:m},c.type==="custom"?C(c.message,c):o?o(c):d.exports.createElement(Ct,{toast:c,position:p}))}))},Mt=u;export{Pt as D,St as I,Mt as _,Ft as a,u as n};
