import{c as r}from"./index.0579b088.js";import{j as a}from"./app.75cef797.js";function b({children:t,...e}){return a("button",{...e,className:r(e!=null&&e.disabled?"bg-slate-600 hover:bg-slate-700 text-sm border py-1.5 px-3 rounded-full text-white border-slate-600":"bg-sky-600 hover:bg-sky-700 text-sm border py-1.5 px-3 rounded-full text-white border-sky-600",e.className),children:t})}export{b as P};
