<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $guarded;

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function shippingaddress()
    {
        return $this->belongsTo(Shippingaddress::class);
    }

    public function paymentmethod()
    {
        return $this->belongsTo(Paymentmethod::class);
    }
}
