<?php

namespace App\Http\Controllers;

use App\Models\Shippingmethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ShippingmethodController extends Controller
{
    public function store(Request $request)
    {

        $done = Shippingmethod::where('user_id', Auth::user()->id)->count();
        if ($done > 0) {
            $data = Shippingmethod::where('user_id', Auth::user()->id)->first();
            $data->update([
                'title' => $request->_title,
                'price' => $request->_price,
                'note' => $request->_note,
            ]);
            return Redirect::back()->with('success', 'Metode pembayaran berhasil ditambahkan');
        }

        Shippingmethod::create([
            'user_id'   => Auth::user()->id,
            'title' => $request->_title,
            'price' => $request->_price,
            'note' => $request->_note,
        ]);
        return Redirect::back()->with('success', 'Metode pembayaran berhasil ditambahkan');
    }
}
