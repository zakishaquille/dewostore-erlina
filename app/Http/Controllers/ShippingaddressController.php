<?php

namespace App\Http\Controllers;

use App\Models\Shippingaddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ShippingaddressController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->primary) {
            Shippingaddress::where('user_id', Auth::user()->id)->update([
                'primary'           => 'no'

            ]);
        }

        $primary = Shippingaddress::where('user_id', Auth::user()->id)->count();
        $primary = $primary > 0;
        if ($primary && !$request->primary) {
            $primary = false;
        } elseif (!$primary && $request->primary) {
            $primary = true;
        }
        $primary = $primary ? 'yes' : 'no';


        Shippingaddress::create([
            'user_id'           => Auth::user()->id,
            'recipientname'     => $request->recipientname,
            'phone'             => $request->phone,
            'province_code'     => $request->province_code,
            'province_desc'     => $request->province_desc,
            'city_code'         => $request->city_code,
            'city_desc'         => $request->city_desc,
            'district_code'     => $request->district_code,
            'district_desc'     => $request->district_desc,
            'postalcode'        => $request->postalcode,
            'address'           => $request->address,
            'primary'           => $primary,
        ]);
        return Redirect::back()->with('success', 'Alamat pengiriman berhasil ditambahkan');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shippingaddress  $shippingaddress
     * @return \Illuminate\Http\Response
     */
    public function update(Shippingaddress $shippingaddress)
    {
        Shippingaddress::where('user_id', Auth::user()->id)->update([
            'primary'           => 'no'

        ]);
        $shippingaddress->update([
            'primary'           => 'yes',
        ]);
        return Redirect::back()->with('success', 'Alamat utama pengiriman berhasil update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shippingaddress  $shippingaddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shippingaddress $shippingaddress)
    {
        $shippingaddress->delete();
        return Redirect::back()->with('success', 'Alamat pengiriman berhasil di hapus');
    }
}
