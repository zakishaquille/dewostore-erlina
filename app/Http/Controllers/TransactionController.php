<?php

namespace App\Http\Controllers;

use App\Http\Resources\TransactionListResources;
use App\Models\Cart;
use App\Models\Paymentmethod;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Support\Str;

class TransactionController extends Controller
{
    public function list()
    {
        return inertia('Transaction/List');
    }

    public function history()
    {
        return inertia('Transaction/History', [
            'history1'       => TransactionListResources::collection(Transaction::whereStatus(1)->get()),
            'history2'       => TransactionListResources::collection(Transaction::whereStatus(2)->get()),
            'history3'       => TransactionListResources::collection(Transaction::whereStatus(3)->get()),
            'history4'       => TransactionListResources::collection(Transaction::whereStatus(4)->get()),
        ]);
    }

    public function store(Request $request)
    {
        $resi = date('ddmmyy') . Str::random(5);
        foreach ($request->product as $key) {
            Cart::where(['user_id' => Auth::user()->id, 'product_id' =>  $key['product_id']])->delete();
            Transaction::insert([
                'user_id'   => Auth::user()->id,
                'paymentmethod_id' => $request->paymentmethod_id,
                'shippingaddress_id' => $request->shippingaddress_id,
                'shippingmethod_id' => $request->shippingmethod_id,
                'product_id' => $key['product_id'],
                'resi' => $resi,
                'qty'   => $key['quantity'],
                'total' => $request->total_price,
                'paymentmethod_name' => $request->paymentmethod_name,
                'paymentmethod_logo' => $request->paymentmethod_logo,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
        $py =  Paymentmethod::find($request->paymentmethod_id);
        return Redirect::route('transaction.thankyou', ['resi' => $resi, 'date' => date('d F Y'), 'methode' => $py->name])->with('success', "Berhasil");
    }

    public function thankyou(Request $request)
    {
        return inertia('Transaction/Thanks', [
            'resi'  => $request->query('resi'),
            'date'  => $request->query('date'),
            'methode'  => $request->query('methode'),
        ]);
    }

    public function updatestatus(Request $request)
    {
        $data = Transaction::where('resi', $request->resi)->get();
        foreach ($data as $key) {
            $key->update([
                'status' => $request->statusNumber
            ]);
        }
        return Redirect::back()->with('success', $request->message);
    }
}
