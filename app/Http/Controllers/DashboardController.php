<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index()
    {
        if(Auth::user()->role === "buyer"){
            return Redirect::route('base');
        }else{
            return Inertia::render('Dashboard');
        }
    }
}
