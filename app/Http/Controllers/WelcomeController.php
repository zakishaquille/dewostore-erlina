<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;


class WelcomeController extends Controller
{
    public function index()
    {
        $minuman    = Product::whereType('minuman')->limit(5)->get();
        $snack      = Product::whereType('snack')->limit(5)->get();
        $jamu       = Product::whereType('jamu')->limit(5)->get();
        return Inertia::render('Welcome', [
            'canLogin'      => Route::has('login'),
            'canRegister'   => Route::has('register'),
            'minuman'       => $minuman,
            'snack'         => $snack,
            'jamu'          => $jamu
        ]);
    }
}
