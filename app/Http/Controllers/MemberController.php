<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function list()
    {
        return inertia('Member/List', [
            'members' => User::get()
        ]);
    }
}
