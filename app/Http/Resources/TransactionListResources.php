<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionListResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $array                          = parent::toArray($request);
        $array['product']               = $this->product;
        $array['shippingaddress']       = $this->shippingaddress;
        $array['paymentmethod']         = $this->paymentmethod;
        return $array;
    }
}
