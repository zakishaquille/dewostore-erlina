<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartListResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $array                          =  parent::toArray($request);
        $array['product_title']         = $this->product->title;
        $array['product_subtitle']      = $this->product->subtitle;
        $array['product_slug']          = $this->product->slug;
        $array['product_image']         = $this->product->image;
        $array['product_stock']         = $this->product->stock;
        return $array;
    }
}
