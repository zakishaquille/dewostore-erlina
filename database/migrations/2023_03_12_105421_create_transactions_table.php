<?php

use App\Models\Paymentmethod;
use App\Models\Product;
use App\Models\Shippingaddress;
use App\Models\Shippingmethod;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class);
            $table->foreignIdFor(Product::class);
            $table->foreignIdFor(Paymentmethod::class);
            $table->foreignIdFor(Shippingaddress::class);
            $table->foreignIdFor(Shippingmethod::class);
            $table->string('resi');
            $table->string('qty');
            $table->string('total');
            $table->string('status')->default('1');
            $table->string('paymentmethod_name');
            $table->string('paymentmethod_logo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
