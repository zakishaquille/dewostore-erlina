<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title'             => $this->faker->sentence(4),
            'slug'              => Str::random(20),
            'subtitle'          => $this->faker->sentence(6),
            'description'       => $this->faker->paragraph(20),
            'type'              => $this->faker->randomElement(['minuman', 'snack', 'jamu']),
            'image'             => $this->faker->imageUrl(),
            'amount'            => $this->faker->numberBetween(10000, 100000),
            'stock'             => $this->faker->numberBetween(0, 100),
        ];
    }
}
