<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\PaymentmethodController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShippingaddressController;
use App\Http\Controllers\ShippingmethodController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WelcomeController::class, 'index'])->name('base');
Route::get('product/{product:slug}', [ProductController::class, 'show'])->name('product.show');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::post('update-status', [TransactionController::class, 'updatestatus'])->name('updatestatus');

    Route::prefix('transaction')->group(function () {
        Route::get('list', [TransactionController::class, 'list'])->name('transaction.list');
        Route::get('history', [TransactionController::class, 'history'])->name('transaction.history');
        Route::post('store', [TransactionController::class, 'store'])->name('transaction.store');
        Route::get('thankyou', [TransactionController::class, 'thankyou'])->name('transaction.thankyou');
    });

    Route::prefix('products')->group(function () {
        Route::get('list', [ProductController::class, 'list'])->name('product.list');
    });

    Route::prefix('member')->group(function () {
        Route::get('list', [MemberController::class, 'list'])->name('member.list');
    });

    // member
    Route::prefix('cart')->group(function () {
        Route::get('my-carts', [CartController::class, 'my'])->name('my.carts');
        Route::post('cart', [CartController::class, 'store'])->name('cart.store');
        Route::delete('cart/{cart}', [CartController::class, 'destroy'])->name('cart.destroy');
    });

    Route::post('shiping-method', [ShippingmethodController::class, 'store'])->name('shiping-method.store');

    Route::post('shiping-address', [ShippingaddressController::class, 'store'])->name('shiping-address.store');
    Route::post('shiping-address-up/{shippingaddress}', [ShippingaddressController::class, 'update'])->name('shiping-address.update');
    Route::delete('shiping-address-del/{shippingaddress}', [ShippingaddressController::class, 'destroy'])->name('shiping-address.destroy');
    // Payment method
    Route::post('payment-method', [PaymentmethodController::class, 'store'])->name('paymentmethod.store');
});
