import React from 'react';
import AppLayout from '@/Layouts/AppLayout';
import { Image } from 'antd';


interface PropsData {
    members: any
}

export default function List(props: PropsData) {
    return (
        <AppLayout
            title="List"
        >
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-xl sm:rounded-lg p-2">
                        {props?.members?.map((dt: any, index: number) => {
                            return (
                                <div key={index}>
                                    <div>
                                        <div>{dt?.name}</div>
                                        <div>{dt?.email}</div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        </AppLayout>
    );
}
