import React from 'react';
import AppLayout from '@/Layouts/AppLayout';
import { Image } from 'antd';


interface PropsData {
    products: any
}

export default function List(props: PropsData) {
    return (
        <AppLayout
            title="List"
        >
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                        {props?.products?.map((dt: any, index: number) => {
                            return (
                                <div key={index} className='flex justify-start items-start gap-2'>
                                    <Image width={200} height="auto" src={dt?.image} alt="img" />
                                    <div>
                                        <div>{dt?.title}</div>
                                        <div>{dt?.stock}</div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        </AppLayout>
    );
}
