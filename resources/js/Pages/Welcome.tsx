import { InertiaLink, usePage } from '@inertiajs/inertia-react';
import React from 'react';
import useRoute from '@/Hooks/useRoute';
import useTypedPage from '@/Hooks/useTypedPage';
import { Head } from '@inertiajs/inertia-react';
import TextInput from '@/Components/TextInput';
import CardProduct from '@/Components/CardProduct';
import BaseLayout from '@/Layouts/BaseLayout';

interface Props {
    minuman: any;
    snack: any;
    jamu: any;
}

export default function Welcome({
    minuman,
    snack,
    jamu
}: Props) {

    const page = usePage();
    console.log(page?.props?.cart);

    return (
        <BaseLayout>
            <div className='max-w-7xl mx-auto pt-10'>
                <div className='bg-slate-200'>
                    <img className='h-72 w-full rounded-xl' src="/main-banner.jpg" alt="drink-banner" />
                </div>
                <div className='mt-10'>
                    <div className='font-bold capitalize'>Minuman</div>
                    <div className='flex items-start gap-4'>
                        <div className='w-2/12'>
                            <div className='bg-slate-200 h-72 rounded-lg w-full'>
                                <img src="/FinalDrinkBanner.png" alt="drink-banner" />
                            </div>
                        </div>
                        <div className='w-10/12 grid grid-cols-5 gap-4'>
                            {minuman?.map((dt: any, index: number) => {
                                return (
                                    <CardProduct
                                        key={index}
                                        image={dt?.image}
                                        slug={dt?.slug}
                                        title={dt?.title}
                                        subtitle={dt?.subtitle}
                                        amount={dt?.amount}
                                    />
                                )
                            })}
                        </div>
                    </div>
                </div>
                <div className='mt-20'>
                    <div className='font-bold capitalize'>Makanan Ringan</div>
                    <div className='flex items-start gap-4'>
                        <div className='w-2/12'>
                            <div className='bg-slate-200 h-72 rounded-lg w-full'>
                                <img src="/FinalChipsBanner.png" alt="chips-banner" />
                            </div>
                        </div>
                        <div className='w-10/12 grid grid-cols-5 gap-4'>
                            {snack?.map((dt: any, index: number) => {
                                return (
                                    <CardProduct
                                        key={index}
                                        image={dt?.image}
                                        slug={dt?.slug}
                                        title={dt?.title}
                                        subtitle={dt?.subtitle}
                                        amount={dt?.amount}
                                    />
                                )
                            })}
                        </div>
                    </div>
                </div>
                <div className='mt-20'>
                    <div className='font-bold capitalize'>Jamu Instan</div>
                    <div className='flex items-start gap-4'>
                        <div className='w-2/12'>
                            <div className='bg-slate-200 h-72 rounded-lg w-full'>
                                <img src="/FinalJamuInstantBanner.png" alt="jamu-banner" />
                            </div>
                        </div>
                        <div className='w-10/12 grid grid-cols-5 gap-4'>
                            {jamu?.map((dt: any, index: number) => {
                                return (
                                    <CardProduct
                                        key={index}
                                        image={dt?.image}
                                        slug={dt?.slug}
                                        title={dt?.title}
                                        subtitle={dt?.subtitle}
                                        amount={dt?.amount}
                                    />
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>

            {/* <div className='bg-sky-200 py-4 mt-20'>
                <div className='text-center font-bold text-lg text-sky-700'>Layanan spesial dari kami hany untukmu.</div>
            </div> */}
        </BaseLayout>
    );
}
