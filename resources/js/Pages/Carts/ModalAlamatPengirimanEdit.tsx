import Checkbox from '@/Components/Checkbox'
import FormInput from '@/Components/FormInput'
import FormSelect from '@/Components/FormSelect'
import InputError from '@/Components/InputError'
import InputLabel from '@/Components/InputLabel'
import Modal from '@/Components/Modal'
import PrimaryButton from '@/Components/PrimaryButton'
import SecondaryButton from '@/Components/SecondaryButton'
import TextInput from '@/Components/TextInput'
import useRoute from '@/Hooks/useRoute'
import { InertiaLink, useForm } from '@inertiajs/inertia-react'
import React, { useEffect, useState } from 'react'
import { TbMap2, TbPlus, TbTrash } from 'react-icons/tb'

interface Props {
    isOpen: boolean,
    onClose: (e: boolean) => void
    data: any
}

const ModalAlamatPengirimanEdit = (props: Props) => {
    const route = useRoute();
    const form = useForm({
        'recipientname': '',
        'phone': '',
        'province_code': '',
        'province_desc': '',
        'city_code': '',
        'city_desc': '',
        'district_code': '',
        'district_desc': '',
        'postalcode': '',
        'address': '',
        'primary': false,
    });

    function onSubmit(e: React.FormEvent) {
        e.preventDefault();
        form.post(route('shiping-address.store'), {
            onFinish: () => props?.onClose(false)
        });
    }

    const [provinces, setProvinces] = useState<any>([])
    useEffect(() => {
        const find = () => {
            return provinces.filter((dt: any) => dt?.value === form.data.province_code)
        }
        form.setData("province_desc", find()[0]?.label)
    }, [form.data.province_code])

    useEffect(() => {
        fetch('/indonesia/provinces.json')
            .then(response => response.json())
            .then(data => {
                const newArray = data.map((item: any) => {
                    return {
                        value: item.id,
                        label: item.name
                    }
                });
                setProvinces(newArray)
            })
    }, [])

    const [city, setCity] = useState<any[]>([])
    useEffect(() => {
        const find = () => {
            return city.filter((dt: any) => dt?.value === form.data.city_code)
        }
        form.setData("city_desc", find()[0]?.label)
    }, [form.data.city_code])

    useEffect(() => {
        fetch('/indonesia/regencies.json')
            .then(response => response.json())
            .then(data => {
                const newArray = data.filter((dt: any) => dt?.province_id === form.data.province_code).map((item: any) => {
                    return {
                        value: item.id,
                        label: item.name,
                    }
                });
                setCity(newArray)
            })
    }, [form.data.province_code])


    const [district, setDistrict] = useState<any[]>([])
    useEffect(() => {
        const find = () => {
            return district.filter((dt: any) => dt?.value === form.data.district_code)
        }
        form.setData("district_desc", find()[0]?.label)
    }, [form.data.district_code])

    useEffect(() => {
        fetch('/indonesia/districts.json')
            .then(response => response.json())
            .then(data => {
                const newArray = data.filter((dt: any) => dt?.regency_id === form.data.city_code).map((item: any) => {
                    return {
                        value: item.id,
                        label: item.name,
                    }
                });
                setDistrict(newArray)
            })
    }, [form.data.city_code])

    const [formShow, setFormShow] = useState<boolean>(false);

    useEffect(() => {
        if (props?.data?.length > 0) {
            setFormShow(true);
        }
    }, [props?.data]);

    const setChangePrimary = (id: number) => {
        form.post(route('shiping-address.update', id));
    }
    return (
        <>
            <Modal isOpen={props?.isOpen} onClose={() => props?.onClose(false)}>
                <div className='p-4 text-sm'>
                    {formShow ? (
                        <>
                            <div className="text-center font-bold">Daftar Alamat</div>
                            <div className="text-center font-bold absolute top-4 right-4">
                                <button onClick={() => setFormShow(!formShow)}>
                                    <TbPlus className='w-6 h-6 text-blue-600' />
                                </button>
                            </div>
                            <div className='mt-4'>
                                {props?.data?.map((dt: any, index: number) => {
                                    return (
                                        <div key={index} className="bg-white border border-slate-100 rounded-xl p-4 shadow-lg mb-2 relative">
                                            <div className="font-bold flex items-center gap-4 mb-4"><TbMap2 className="w-5 h-5" /> <span>Alamat Pengiriman</span></div>
                                            <div className="font-bold capitalize flex items-center">{dt?.recipientname} | {dt?.phone} {dt?.primary === "yes" && <span className="ml-4 bg-blue-600 py-1 px-2 rounded text-white text-xs">Utama</span>}</div>
                                            <div className="capitalize mt-3">{dt?.address}</div>
                                            <div>{dt?.district_desc}, {dt?.city_desc}, {dt?.province_desc}</div>
                                            {dt?.primary === "yes" ? (
                                                <div className='mt-4'>
                                                    <PrimaryButton disabled>Alamat Terpilih</PrimaryButton>
                                                </div>
                                            ) : (
                                                <div className='mt-4'>
                                                    <PrimaryButton onClick={() => setChangePrimary(dt?.id)}>Pilih Alamat</PrimaryButton>
                                                </div>
                                            )}
                                            {dt?.primary === "no" && (
                                                <div className="text-center font-bold absolute top-4 right-4">
                                                    <InertiaLink method='delete' href={route('shiping-address.destroy', dt?.id)}>
                                                        <TbTrash className='w-6 h-6 text-slate-400' />
                                                    </InertiaLink>
                                                </div>
                                            )}
                                        </div>
                                    )
                                })}
                            </div>
                        </>
                    ) : (
                        <>
                            <div className="text-center font-bold">Alamat Pengiriman</div>
                            <form onSubmit={onSubmit}>
                                <div className='my-2'>
                                    <FormInput form={form} label="Masukkan nama penerima*" name="recipientname" />
                                </div>
                                <div className='my-2'>
                                    <FormInput min={9} max={13} form={form} label="No. Telephone" name="phone" />
                                </div>
                                <div className='my-2'>
                                    <FormSelect
                                        form={form}
                                        value={form.data.province_code}
                                        data={provinces}
                                        name="province_code"
                                        label="Provinsi*"
                                    />
                                </div>
                                <div className='my-2'>
                                    <FormSelect
                                        form={form}
                                        value={form.data.city_code}
                                        data={city}
                                        name="city_code"
                                        label="Kabupaten atau Kota*"
                                    />
                                </div>
                                <div className='my-2'>
                                    <FormSelect
                                        form={form}
                                        value={form.data.district_code}
                                        data={district}
                                        name="district_code"
                                        label="Kecamatan*"
                                    />
                                </div>
                                <div className='my-2'>
                                    <FormInput min={5} max={5} form={form} label="Kode Pos" name="postalcode" />
                                </div>
                                <div className='my-2'>
                                    <FormInput form={form} label="Alamat Lengkap" name="address" />
                                </div>
                                <div className='flex justify-between items-center mt-4'>
                                    <div>Jadikan alamat utama</div>
                                    <Checkbox
                                        name="primary"
                                        id="primary"
                                        checked={form.data.primary}
                                        onChange={e => form.setData('primary', e.currentTarget.checked)}
                                    />
                                </div>
                                <div className='flex justify-between items-center mt-4'>

                                    <SecondaryButton onClick={() => setFormShow(!formShow)} className='mt-4' type='button'>Cancel</SecondaryButton>
                                    <PrimaryButton className='mt-4' type='submit'>Simpan Alamat</PrimaryButton>
                                </div>
                            </form>
                        </>
                    )}
                </div>
            </Modal>
        </>
    )
}

export default ModalAlamatPengirimanEdit
