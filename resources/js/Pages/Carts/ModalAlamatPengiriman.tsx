import Checkbox from '@/Components/Checkbox'
import FormInput from '@/Components/FormInput'
import FormSelect from '@/Components/FormSelect'
import InputError from '@/Components/InputError'
import InputLabel from '@/Components/InputLabel'
import Modal from '@/Components/Modal'
import PrimaryButton from '@/Components/PrimaryButton'
import TextInput from '@/Components/TextInput'
import useRoute from '@/Hooks/useRoute'
import { useForm } from '@inertiajs/inertia-react'
import React, { useEffect, useState } from 'react'

interface Props {
    isOpen: boolean,
    onClose: (e: boolean) => void
}

const ModalAlamatPengiriman = (props: Props) => {
    const route = useRoute();
    const form = useForm({
        'recipientname': '',
        'phone': '',
        'province_code': '',
        'province_desc': '',
        'city_code': '',
        'city_desc': '',
        'district_code': '',
        'district_desc': '',
        'postalcode': '',
        'address': '',
        'primary': false,
    });

    function onSubmit(e: React.FormEvent) {
        e.preventDefault();
        form.post(route('shiping-address.store'), {
            onFinish: () => props?.onClose(false)
        });
    }

    // set data provinsi
    const [provinces, setProvinces] = useState<any>([])
    useEffect(() => {
        const find = () => {
            return provinces.filter((dt: any) => dt?.value === form.data.province_code)
        }
        form.setData("province_desc", find()[0]?.label)
    }, [form.data.province_code])
    // mengeluarkan data provinsi yang ada di indonesia
    useEffect(() => {
        fetch('/indonesia/provinces.json')
            .then(response => response.json())
            .then(data => {
                const newArray = data.map((item: any) => {
                    return {
                        value: item.id,
                        label: item.name
                    }
                });
                setProvinces(newArray) //*
            })
    }, [])

    // ---------------------------------------------

    // set data kota
    const [city, setCity] = useState<any[]>([])
    useEffect(() => {
        const find = () => {
            return city.filter((dt: any) => dt?.value === form.data.city_code)
        }
        form.setData("city_desc", find()[0]?.label)
    }, [form.data.city_code])
    // mengeluarkan data kota berdasarkan dari data provinsi yang diselect
    useEffect(() => {
        fetch('/indonesia/regencies.json')
            .then(response => response.json())
            .then(data => {
                const newArray = data.filter((dt: any) => dt?.province_id === form.data.province_code).map((item: any) => {
                    return {
                        value: item.id,
                        label: item.name,
                    }
                });
                setCity(newArray) //*
            })
    }, [form.data.province_code])

    // ---------------------------------------------

    // set data kecamatan
    const [district, setDistrict] = useState<any[]>([])
    useEffect(() => {
        const find = () => {
            return district.filter((dt: any) => dt?.value === form.data.district_code)
        }
        form.setData("district_desc", find()[0]?.label)
    }, [form.data.district_code])
    // mengeluarkan data kecamatan berdasarkan dari data kota yang diselect
    useEffect(() => {
        fetch('/indonesia/districts.json')
            .then(response => response.json())
            .then(data => {
                const newArray = data.filter((dt: any) => dt?.regency_id === form.data.city_code).map((item: any) => {
                    return {
                        value: item.id,
                        label: item.name,
                    }
                });
                setDistrict(newArray)
            })
    }, [form.data.city_code])

    return (
        <>
            <Modal isOpen={props?.isOpen} onClose={() => props?.onClose(false)}>
                <div className='p-5'>
                    <div className="text-center font-bold">Alamat Pengiriman</div>
                    <form onSubmit={onSubmit}>
                        <div className='my-2'>
                            <FormInput form={form} label="Masukkan nama penerima*" name="recipientname" />
                        </div>
                        <div className='my-2'>
                            <FormInput min={9} max={13} form={form} label="No. Telephone" name="phone" />
                        </div>
                        <div className='my-2'>
                            <FormSelect
                                form={form}
                                value={form.data.province_code}
                                data={provinces}
                                name="province_code"
                                label="Provinsi*"
                            />
                        </div>
                        <div className='my-2'>
                            <FormSelect
                                form={form}
                                value={form.data.city_code}
                                data={city}
                                name="city_code"
                                label="Kabupaten atau Kota*"
                            />
                        </div>
                        <div className='my-2'>
                            <FormSelect
                                form={form}
                                value={form.data.district_code}
                                data={district}
                                name="district_code"
                                label="Kecamatan*"
                            />
                        </div>
                        <div className='my-2'>
                            <FormInput min={5} max={5} form={form} label="Kode Pos" name="postalcode" />
                        </div>
                        <div className='my-2'>
                            <FormInput form={form} label="Alamat Lengkap" name="address" />
                        </div>
                        <div className='flex justify-between items-center mt-4'>
                            <div>Jadikan alamat utama</div>
                            <Checkbox
                                name="primary"
                                id="primary"
                                checked={form.data.primary}
                                onChange={e => form.setData('primary', e.currentTarget.checked)}
                                required
                            />
                        </div>
                        <PrimaryButton className='mt-4' type='submit'>Simpan Alamat</PrimaryButton>
                    </form>
                </div>
            </Modal>
        </>
    )
}

export default ModalAlamatPengiriman
