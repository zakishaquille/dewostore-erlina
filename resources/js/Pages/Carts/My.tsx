import Checkbox from "@/Components/Checkbox";
import Modal from "@/Components/Modal";
import PrimaryButton from "@/Components/PrimaryButton";
import SecondaryButton from "@/Components/SecondaryButton";
import { tsxToIDR } from "@/Components/ToIDR";
import useRoute from "@/Hooks/useRoute";
import BaseLayout from "@/Layouts/BaseLayout";
import { InertiaLink, Link, useForm } from "@inertiajs/inertia-react";
import React, { FunctionComponent, useEffect, useState } from "react";
import { TbChevronDown, TbMap2, TbPlus, TbSquareRoundedMinus, TbSquareRoundedPlus, TbTrash } from "react-icons/tb";
import ModalAlamatPengiriman from "./ModalAlamatPengiriman";
import ModalAlamatPengirimanEdit from "./ModalAlamatPengirimanEdit";
import ModalMetodePembayaran from "./ModalMetodePembayaran";
import { Collapse } from 'antd';

const { Panel } = Collapse;

interface MyProps {
    data: any
    shipping_address: any
    primary: any
    payment_method: any
    shipping_method: any
}

const My: FunctionComponent<MyProps> = (props) => {
    const route = useRoute();
    const { data } = props?.data;
    const [productList, setProductList] = useState(data);
    useEffect(() => {
        setProductList(data);
    }, [data])

    const handleQuantityChange = (index: number, newQuantity: number) => {
        const updatedProductList = [...productList];
        updatedProductList[index].quantity = newQuantity;
        setProductList(updatedProductList);
    }

    const [selectedItems, setSelectedItems] = useState<string[]>([]);
    const handleCheckboxChange = (id: string) => {
        const isSelected = selectedItems.includes(id);
        if (isSelected) {
            setSelectedItems(selectedItems.filter(item => item !== id));
        } else {
            setSelectedItems([...selectedItems, id]);
        }
    }

    const getTotalPrice = () => {
        let totalPrice = 0;
        selectedItems.forEach(id => {
            const item = productList.find((product: any) => product.id === id);
            totalPrice += item.amount * item.quantity;
        })
        return totalPrice;
    }
    const [openAlamatPengiriman, setOpenAlamatPengiriman] = useState<boolean>(false);
    const shipping_address = props?.shipping_address?.filter((dt: any) => dt?.primary === "yes");

    const [openMethodePembayaran, setOpenMethodePembayaran] = useState<boolean>(false);

    const dataTransaction = {
        paymentmethod_id: props?.payment_method?.id || 0,
        shippingaddress_id: props?.primary?.id,
        shippingmethod_id: props?.shipping_method?.id || 0,
        product: productList,
        total_price: getTotalPrice(),
        paymentmethod_name: props?.payment_method?.name,
        paymentmethod_logo: props?.payment_method?.logo
    }

    const isCheck = () => {
        if (shipping_address?.length > 0 && props?.payment_method && props?.shipping_method) {
            return true
        }
        return false
    }

    const [methodeSend, setMethodeSend] = useState<boolean>(false)

    const shipping_method_data = [
        {
            title: 'Kilat',
            price: 30000,
            note: 'Perkiraan sampai hari ini sebelum jam 14:00'
        },
        {
            title: 'Same day service',
            price: 20000,
            note: 'Dikirim hari yang sama sebelum jam 16:00'
        },
        {
            title: 'Reguler',
            price: 10000,
            note: 'Perkiraan sampai 10 hari dari hari ini'
        },
        {
            title: 'Ekonomi',
            price: 5000,
            note: 'Perkiraan sampai 20 hari dari hari ini'
        },
        {
            title: 'Kargo',
            price: 25000,
            note: 'Perkiraan sampai 10 hari dari hari ini, ukuran pembelian lebih dari 30kg'
        },
    ]

    const form = useForm({
        '_title': '',
        '_price': 0,
        '_note': '',
    });

    function onSubmitMethod(e: React.FormEvent) {
        e.preventDefault();
        form.post(route('shiping-method.store'));
        setMethodeSend(false);
    }

    const isCheckMethod = () => {
        if (form.data?._title !== '' && form.data?._price && form.data._note !== '') {
            return true
        }
        return false
    }

    return (
        <BaseLayout>
            <Modal isOpen={methodeSend} onClose={() => setMethodeSend(!methodeSend)}>
                <div className="p-4">
                    {shipping_method_data?.map((dt: any, index: number) => {
                        return (
                            <div key={index} className="my-2">
                                <Collapse
                                    expandIconPosition="right"
                                    collapsible="icon"
                                    onChange={() => {
                                        form.setData({
                                            _title: dt?.title,
                                            _price: dt?.price,
                                            _note: dt?.note
                                        })
                                    }}
                                >
                                    <Panel
                                        header={<div><span className="font-bold">{dt?.title}</span> {`Rp. ${tsxToIDR(dt?.price)}`}</div>}
                                        key={index}
                                    >
                                        <div>{dt?.note}</div>
                                    </Panel>
                                </Collapse>
                            </div>
                        )
                    })}
                    <form onSubmit={onSubmitMethod}>
                        {isCheckMethod() && (
                            <div className="mt-4 pt-4 border-t grid grid-cols-2 items-center">
                                <div>
                                    <div>Dipilih</div>
                                    <div className="text-xs"><strong>{form?.data?._title}</strong> | {form?.data?._price} | {form?.data?._note}</div>
                                </div>
                                <div className="flex justify-end">
                                    <PrimaryButton className="w-max">Submit</PrimaryButton>
                                </div>
                            </div>
                        )}
                    </form>
                </div>
            </Modal>
            {
                shipping_address?.length > 0 ? (
                    <ModalAlamatPengirimanEdit data={props?.shipping_address} isOpen={openAlamatPengiriman} onClose={(e: boolean) => setOpenAlamatPengiriman(e)} />
                ) : (
                    <ModalAlamatPengiriman isOpen={openAlamatPengiriman} onClose={(e: boolean) => setOpenAlamatPengiriman(e)} />
                )
            }
            <ModalMetodePembayaran isOpen={openMethodePembayaran} onClose={(e: boolean) => setOpenMethodePembayaran(e)} />
            {
                productList?.length > 0 ? (
                    <div className="grid grid-cols-3 gap-4 min-h-screen text-xs">
                        <div className="col-span-2">
                            {shipping_address?.length > 0 ?
                                shipping_address.map((dt: any, index: number) => {
                                    return (
                                        <div key={index} className="text-sm bg-white border border-slate-100 rounded-xl p-4 shadow-lg mb-2 relative">
                                            <div className="font-bold flex items-center gap-4"><TbMap2 className="w-5 h-5" /> <span>Alamat Tujuan Pengiriman</span></div>
                                            <div className="font-bold capitalize flex items-center">{dt?.recipientname} | {dt?.phone} <span className="ml-4 bg-sky-600 py-1 px-2 rounded text-white text-xs">Utama</span></div>
                                            <div className="capitalize mt-3">{dt?.address}</div>
                                            <div className="capitalize">{dt?.district_desc?.toLowerCase()}, {dt?.city_desc?.toLowerCase()}, {dt?.province_desc?.toLowerCase()}</div>
                                            <button onClick={() => setOpenAlamatPengiriman(!openAlamatPengiriman)} className="absolute top-4 right-4">
                                                <span className="font-bold text-sky-600">Ubah Alamat</span>
                                            </button>
                                        </div>
                                    )
                                }) : (
                                    <>
                                        <div className="bg-white border border-slate-100 rounded-xl p-4 shadow-lg mb-2">
                                            <div className="font-bold flex items-center gap-4"><TbMap2 className="w-5 h-5" /> <span>Alamat Tujuan Pengiriman</span></div>
                                            <div>Belum ada alamat yang terdaftar.</div>
                                            <SecondaryButton onClick={() => setOpenAlamatPengiriman(!openAlamatPengiriman)} className="flex justify-center gap-2 items-center mt-4">
                                                <TbPlus className="w-4 h-4" />
                                                <span>Buat Alamat Pengiriman</span>
                                            </SecondaryButton>
                                        </div>
                                    </>
                                )}
                            <div className="bg-white border border-slate-100 rounded-xl p-4 shadow-lg">
                                <div className="mb-4 pb-4 border-b font-bold">Keranjang Belanja</div>
                                {productList?.map((dt: any, index: number) => {
                                    return (
                                        <div key={index} className="flex items-start my-4 pb-2 border-b">
                                            <div className="w-2/12 pr-4 flex items-start gap-2">
                                                <div className="w-8">
                                                    <div className="mb-6 flex justify-center">
                                                        <Checkbox onChange={() => handleCheckboxChange(dt.id)} checked={selectedItems.includes(dt.id)} />
                                                    </div>
                                                    {!selectedItems.includes(dt.id) && (
                                                        <div className="flex justify-center">
                                                            <InertiaLink href={route("cart.destroy", dt?.id)} method="delete">
                                                                <TbTrash className="w-5 h-5 text-slate-300 hover:text-red-500" />
                                                            </InertiaLink>
                                                        </div>
                                                    )}
                                                </div>
                                                <img src={dt?.product_image} alt="img" className="w-2/3 rounded-lg" />
                                            </div>
                                            <div className="w-5/12 px-4">
                                                <div>{dt?.product_title}</div>
                                                <div>Rp. {tsxToIDR(dt?.amount)}</div>
                                            </div>
                                            <div className="w-3/12 px-4">
                                                <div className="flex gap-2 items-center my-2">
                                                    <button disabled={dt?.quantity === 1} onClick={() => handleQuantityChange(index, Number(dt?.quantity - 1))}>
                                                        <TbSquareRoundedMinus className="w-5 h-5" />
                                                    </button>
                                                    <div>{dt?.quantity}</div>
                                                    <button disabled={dt?.quantity >= dt?.product_stock} onClick={() => handleQuantityChange(index, Number(dt?.quantity + 1))}>
                                                        <TbSquareRoundedPlus className="w-5 h-5" />
                                                    </button>
                                                    <div className="text-xs">Stok {dt?.product_stock}</div>
                                                </div>
                                            </div>
                                            <div className="w-2/12 text-right pl-4 font-bold text-sky-600">
                                                <div>Rp. {tsxToIDR(dt?.quantity * dt?.amount)}</div>
                                            </div>
                                        </div>
                                    )
                                })}
                                <div className="flex justify-end mt-6">
                                    <div>
                                        <div>Metode pengiriman</div>
                                        <button onClick={() => setMethodeSend(!methodeSend)} className="w-max gap-6 flex justify-between items-center px-4 py-2 border rounded-full mt-2">
                                            {props?.shipping_method === null ? (
                                                <>
                                                    <div>Pilih metode pengiriman</div>
                                                    <TbChevronDown className="w-5 h-5" />
                                                </>
                                            ) : (
                                                <>
                                                    <div className="capitalize"><span className="font-bold">{props?.shipping_method?.title}</span> {tsxToIDR(props?.shipping_method?.price)}</div>
                                                    <TbChevronDown className="w-5 h-5" />
                                                </>
                                            )}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="bg-white border border-slate-100 rounded-xl p-4 shadow-lg text-sm mb-4 ">
                                <div className="flex justify-between items-center mb-4">
                                    <div className="font-bold">Metode Pembayaran</div>
                                    {props?.payment_method && (
                                        <button className="text-sky-600" onClick={() => setOpenMethodePembayaran(!openMethodePembayaran)}>Pilih Lainnya</button>
                                    )}
                                </div>

                                {props?.payment_method ? (
                                    <div className="relative">
                                        <div className='flex justify-start items-center gap-4'>
                                            <img src={props?.payment_method?.logo} alt={props?.payment_method?.name} className="w-12 h-auto" />
                                            <div className='uppercase'>{props?.payment_method?.name}</div>
                                        </div>
                                    </div>
                                ) : (

                                    <button onClick={() => setOpenMethodePembayaran(!openMethodePembayaran)} className="mt-4 flex items-center justify-between py-2 w-full px-4 rounded-full border border-slate-400">
                                        <div>Pilih Metode Pembayaran</div>
                                        <div><TbChevronDown className="w-5 h-5" /></div>
                                    </button>
                                )}
                            </div>
                            <div className="bg-white border border-slate-100 rounded-xl p-4 shadow-lg text-sm">
                                <div className="mb-4 pb-4 font-bold">Rincian Belanja</div>
                                {selectedItems?.length > 0 && (
                                    <div className="grid grid-cols-3 my-2 pb-2 border-b border-dashed">
                                        <div>Produk</div>
                                        <div>Qty</div>
                                        <div className="flex justify-end">Nominal</div>
                                    </div>
                                )}
                                {selectedItems?.map((dt: any, index: number) => {
                                    return (
                                        <div key={index} className="grid grid-cols-3 my-2 pb-2 border-b border-dashed">
                                            <div>
                                                {productList.filter((item: any) => item?.id === dt)[0]?.product_title?.substr(0, 15)}...
                                            </div>
                                            <div>
                                                {productList.filter((item: any) => item?.id === dt)[0]?.quantity}
                                            </div>
                                            <div className="flex justify-end">
                                                {tsxToIDR(productList.filter((item: any) => item?.id === dt)[0]?.quantity * productList.filter((item: any) => item?.id === dt)[0]?.amount)}
                                            </div>
                                        </div>
                                    )
                                })}
                                <div className="flex justify-between font-bold">
                                    <div>Total Biaya Belanja</div>
                                    <div className="font-bold text-sky-600">Rp {tsxToIDR(getTotalPrice())}</div>
                                </div>
                                {getTotalPrice() !== 0 && isCheck() ? (
                                    <div className="mt-4 flex justify-center">
                                        <InertiaLink href={route('transaction.store')} className="w-full" method="post" data={dataTransaction}>
                                            <PrimaryButton className="w-full">Checkout</PrimaryButton>
                                        </InertiaLink>
                                    </div>
                                ) : (
                                    <div className="mt-4 flex justify-center">
                                        <div className="w-full">
                                            <PrimaryButton disabled className="w-full">Checkout</PrimaryButton>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                ) : (
                    <div className="grid grid-cols-2">
                        <div className="flex justify-center items-center">
                            <img src="/berdebu.png" alt="img" />
                        </div>
                        <div className="flex justify-center items-center">
                            <div>
                                <div className="text-lg font-bold text-center">Tas Belanja Kamu Berdebu!</div>
                                <div className="text-center">Tas belanja kamu masih kosong lho! Mulai belanja sekarang dan dapatkan barang yang kamu inginkan hanya di Gramedia.</div>
                                <div className="flex justify-center mt-4">
                                    <Link href="/">
                                        <PrimaryButton>
                                            Mulai Belanja
                                        </PrimaryButton>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
        </BaseLayout >
    );
}

export default My;
