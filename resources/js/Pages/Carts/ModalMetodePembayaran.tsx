import React from 'react'
import Modal from '@/Components/Modal'
import useRoute from '@/Hooks/useRoute'
import { TbChevronRight } from 'react-icons/tb'
import { InertiaLink } from '@inertiajs/inertia-react'

interface Props {
    isOpen: boolean,
    onClose: (e: boolean) => void
}

const ModalMetodePembayaran = (props: Props) => {
    const route = useRoute();

    const paymentMethodEM = [
        {
            name: "ovo",
            code: "ovo",
            logo: "/logo/ovo.png",
            className: "w-auto h-3"
        },
        {
            name: "gopay",
            code: "gopay",
            logo: "/logo/gopay.png",
            className: "w-auto h-8"
        }
    ]
    const paymentMethodIB = [
        {
            name: "M-Banking BCA",
            code: "mbca",
            logo: "/logo/bca.png"
        },
    ]
    return (
        <>
            <Modal isOpen={props?.isOpen} onClose={() => props?.onClose(false)}>
                <div className='p-5'>
                    <div className="text-center font-bold">Alamat Pengiriman</div>
                    <div className='font-bold mt-3'>Uang Elektronik</div>
                    {paymentMethodEM?.map((dt: any, index: number) => {
                        return (
                            <InertiaLink key={index} onClick={() => props?.onClose(false)} method='post' href={route('paymentmethod.store')} data={dt} className='flex justify-between items-center w-full bg-white shadow-lg rounded-lg mb-4 p-4'>
                                <div className='flex justify-start items-center gap-4'>
                                    <div>
                                        <img src={dt?.logo} alt={dt?.name} className={dt?.className} />
                                    </div>
                                    <div className='uppercase'>{dt?.name}</div>
                                </div>
                                <div>
                                    <TbChevronRight className='w-5 h-5' />
                                </div>
                            </InertiaLink>
                        )
                    })}
                    <div className='font-bold'>Internet Banking</div>
                    {paymentMethodIB?.map((dt: any, index: number) => {
                        return (
                            <InertiaLink key={index} onClick={() => props?.onClose(false)} method='post' href={route('paymentmethod.store')} data={dt} className='flex justify-between items-center w-full bg-white shadow-lg rounded-lg mb-4 p-4'>
                                <div className='flex justify-start items-center gap-4'>
                                    <img src={dt?.logo} alt={dt?.name} className="w-auto h-8" />
                                    <div className='uppercase'>{dt?.name}</div>
                                </div>
                                <div>
                                    <TbChevronRight className='w-5 h-5' />
                                </div>
                            </InertiaLink>
                        )
                    })}
                </div>
            </Modal>
        </>
    )
}

export default ModalMetodePembayaran
