import AppLayout from "@/Layouts/AppLayout";
import BaseLayout from "@/Layouts/BaseLayout";
import { Tabs, TabsProps } from "antd";
import React, { FunctionComponent } from "react";
import moment from "moment";
import 'moment/locale/id';
import { tsxToIDR } from "@/Components/ToIDR";
import { Button, Collapse } from "antd";
import _ from "lodash";
import PrimaryButton from "@/Components/PrimaryButton";
import { InertiaLink, Link } from "@inertiajs/inertia-react";
import { TbLogout } from "react-icons/tb";
import useRoute from "@/Hooks/useRoute";

moment.locale('id');

const { Panel } = Collapse;

interface Transaction {
    id: number;
    resi: string;
    created_at: string;
    total: number;
    product: any;
    shippingaddress: any;
    paymentmethod: any;
    status: any;
}

interface HistoryProps {
    transactions: Transaction[];
}


interface HistoryProps {
    history1: any
    history2: any
    history3: any
    history4: any
}

const History: FunctionComponent<HistoryProps> = (props) => {
    const route = useRoute();
    const data1 = props?.history1?.data;
    const groups1 = _.groupBy(data1, "resi");

    const data2 = props?.history2?.data;
    const groups2 = _.groupBy(data2, "resi");

    const data3 = props?.history3?.data;
    const groups3 = _.groupBy(data3, "resi");

    const data4 = props?.history4?.data;
    const groups4 = _.groupBy(data4, "resi");

    // Render a summary for each group
    const renderGroupSummary = (group: Transaction[]) => {
        const first = group[0];
        const created = moment(first.created_at).format("DD MMMM YYYY");
        return (
            <div className="flex justify-between items-center">
                <div>
                    <div>RESI : {first?.resi}</div>
                    <div>{`Tanggal: ${created}`}</div>
                </div>
                <div>{`Total: Rp${tsxToIDR(first?.total)}`}</div>
                <div>{status(first?.status)}</div>
            </div>
        );
    };

    // Render the details for each group
    const renderGroupDetails = (group: Transaction[]) => {
        return (
            <div className="p-4 bg-white">
                {group.map((t: any, index: number) => (
                    <div key={t.id} className="mb-2 border-b border-dashed pb-3">
                        <div className="font-bold flex items-center gap-2">
                            <div>{index + 1}.</div>
                            <div>{t?.product?.title}</div>
                        </div>
                        <div>QTY: {t?.qty}</div>
                        <div>{`Total: Rp${tsxToIDR(t.product?.amount)}`}</div>
                    </div>
                ))}
            </div>
        );
    };

    const status = (check: any) => {
        switch (check) {
            case "1":
                return <div className="bg-blue-300 px-2 rounded-full text-white">Belum Dibayar</div>
            case "2":
                return <div className="bg-orange-300 px-2 rounded-full text-white">Aktif</div>
            case "3":
                return <div className="bg-green-300 px-2 rounded-full text-white">Selesai</div>
            default:
                return <div className="bg-red-300 px-2 rounded-full text-white">Cancel</div>
        }
    }

    const buttonKlik = (check: any, group: any) => {
        const first = group[0];
        switch (check) {
            case "1":
                return (
                    <InertiaLink method="post" data={{
                        resi: first?.resi,
                        statusNumber: '2',
                        message: "Terimakasih, Pesanan telah terbayar"
                    }} href={route('updatestatus')}>
                        <PrimaryButton className="w-full">Bayar Pesanan</PrimaryButton>
                    </InertiaLink>
                )
            case "2":
                return (
                    <InertiaLink method="post" data={{
                        resi: first?.resi,
                        statusNumber: '3',
                        message: "Pesanan telah diterima"
                    }} href={route('updatestatus')}>
                        <PrimaryButton className="w-full">Pesanan Diterima</PrimaryButton>
                    </InertiaLink>
                )
            case "3":
                return <div className="bg-green-300 px-3 rounded-full text-white">Selesai</div>
            default:
                return <div className="bg-red-300 px-3 rounded-full text-white">Cancel</div>
        }
    }

    const items: TabsProps['items'] = [
        {
            key: '1',
            label: `Belum Dibayar`,
            children: (
                <>
                    {Object.values(groups1)?.length > 0 ? (
                        <Collapse accordion>
                            {Object.values(groups1).map((group, index) => (
                                <Panel
                                    key={index}
                                    header={renderGroupSummary(group)}
                                    className="mb-2"
                                >
                                    {renderGroupDetails(group)}
                                    <div className="mt-2 pt-2 border-t">
                                        <div className="font-bold flex items-center gap-4"><span>Alamat Tujuan Pengiriman</span></div>
                                        <div className="capitalize flex items-center">{group[0]?.shippingaddress?.recipientname} | {group[0]?.shippingaddress?.phone} </div>
                                        <div className="capitalize">{group[0]?.shippingaddress?.address}</div>
                                        <div className="capitalize">{group[0]?.shippingaddress?.district_desc?.toLowerCase()}, {group[0]?.shippingaddress?.city_desc?.toLowerCase()}, {group[0]?.shippingaddress?.province_desc?.toLowerCase()}</div>

                                    </div>
                                    <div className="mt-2 pt-2 border-t">
                                        <div className="font-bold flex items-center gap-4"><span>Metode Pembayaran</span></div>
                                        <div className="flex gap-4 items-start justify-between">
                                            <div className="flex justify-cen items-center gap-4">
                                                <div className="flex gap-4 items-center">
                                                    <img src={group[0]?.paymentmethod_logo} className="w-14 h-auto" />
                                                    {/* <div className="capitalize">{group[0]?.paymentmethod_name}</div> */}
                                                </div>
                                                <div>
                                                    <div>Selesaikan pembayaran Anda sebelum Senin, {moment(group[0]?.paymentmethod?.created).add(1, "day").format("DD MMMM YYYY HH:mm:ss")}</div>
                                                    <div>Metode pembayaran menggunakan <span className="font-bold">{group[0]?.paymentmethod_name}</span></div>
                                                </div>
                                            </div>
                                            <div>
                                                {buttonKlik(group[0]?.status, group)}
                                            </div>
                                        </div>
                                    </div>
                                </Panel>
                            ))}
                        </Collapse>
                    ) : (
                        <Zonk />
                    )}
                </>
            ),
        },
        {
            key: '2',
            label: `Transaksi Aktif`,
            children: (
                <>
                    {Object.values(groups2)?.length > 0 ? (
                        <Collapse accordion>
                            {Object.values(groups2).map((group, index) => (
                                <Panel
                                    key={index}
                                    header={renderGroupSummary(group)}
                                    className="mb-2"
                                >
                                    {renderGroupDetails(group)}
                                    <div className="mt-2 pt-2 border-t">
                                        <div className="font-bold flex items-center gap-4"><span>Alamat Tujuan Pengiriman</span></div>
                                        <div className="capitalize flex items-center">{group[0]?.shippingaddress?.recipientname} | {group[0]?.shippingaddress?.phone} </div>
                                        <div className="capitalize">{group[0]?.shippingaddress?.address}</div>
                                        <div className="capitalize">{group[0]?.shippingaddress?.district_desc?.toLowerCase()}, {group[0]?.shippingaddress?.city_desc?.toLowerCase()}, {group[0]?.shippingaddress?.province_desc?.toLowerCase()}</div>

                                    </div>
                                    <div className="mt-2 pt-2 border-t">
                                        <div className="font-bold flex items-center gap-4"><span>Metode Pembayaran</span></div>
                                        <div className="flex gap-4 items-start justify-between">
                                            <div className="flex justify-cen items-center gap-4">
                                                <div className="flex gap-4 items-center">
                                                    <img src={group[0]?.paymentmethod_logo} className="w-14 h-auto" />
                                                    {/* <div className="capitalize">{group[0]?.paymentmethod_name}</div> */}
                                                </div>
                                                <div>
                                                    {/* <div>Selesaikan pembayaran Anda sebelum Senin, {moment(group[0]?.paymentmethod?.created).add(1, "day").format("DD MMMM YYYY HH:mm:ss")}</div> */}
                                                    <div>Metode pembayaran menggunakan <span className="font-bold">{group[0]?.paymentmethod_name}</span></div>
                                                </div>
                                            </div>
                                            <div>
                                                {buttonKlik(group[0]?.status, group)}
                                            </div>
                                        </div>
                                    </div>
                                </Panel>
                            ))}
                        </Collapse>
                    ) : (
                        <Zonk />
                    )}
                </>
            )
        },
        {
            key: '3',
            label: `Transaksi Selesai`,
            children: (
                <>
                    {Object.values(groups3)?.length > 0 ? (
                        <Collapse accordion>
                            {Object.values(groups3).map((group, index) => (
                                <Panel
                                    key={index}
                                    header={renderGroupSummary(group)}
                                    className="mb-2"
                                >
                                    {renderGroupDetails(group)}
                                    <div className="mt-2 pt-2 border-t">
                                        <div className="font-bold flex items-center gap-4"><span>Alamat Tujuan Pengiriman</span></div>
                                        <div className="capitalize flex items-center">{group[0]?.shippingaddress?.recipientname} | {group[0]?.shippingaddress?.phone} </div>
                                        <div className="capitalize">{group[0]?.shippingaddress?.address}</div>
                                        <div className="capitalize">{group[0]?.shippingaddress?.district_desc?.toLowerCase()}, {group[0]?.shippingaddress?.city_desc?.toLowerCase()}, {group[0]?.shippingaddress?.province_desc?.toLowerCase()}</div>

                                    </div>
                                    <div className="mt-2 pt-2 border-t">
                                        <div className="font-bold flex items-center gap-4"><span>Metode Pembayaran</span></div>
                                        <div className="flex gap-4 items-start justify-between">
                                            <div className="flex justify-cen items-center gap-4">
                                                <div className="flex gap-4 items-center">
                                                    <img src={group[0]?.paymentmethod_logo} className="w-14 h-auto" />
                                                    {/* <div className="capitalize">{group[0]?.paymentmethod_name}</div> */}
                                                </div>
                                                <div>
                                                    <div>Metode pembayaran menggunakan <span className="font-bold">{group[0]?.paymentmethod_name}</span></div>
                                                </div>
                                            </div>
                                            <div>
                                                {buttonKlik(group[0]?.status, group)}
                                            </div>
                                        </div>
                                    </div>
                                </Panel>
                            ))}
                        </Collapse>
                    ) : (
                        <Zonk />
                    )}
                </>
            )
        },
        {
            key: '4',
            label: `Transaksi Dibatalkan`,
            children: (
                <>
                    {Object.values(groups4)?.length > 0 ? (
                        <Collapse accordion>
                            {Object.values(groups4).map((group, index) => (
                                <Panel
                                    key={index}
                                    header={renderGroupSummary(group)}
                                    className="mb-2"
                                >
                                    {renderGroupDetails(group)}
                                    <div className="mt-2 pt-2 border-t">
                                        <div className="font-bold flex items-center gap-4"><span>Alamat Tujuan Pengiriman</span></div>
                                        <div className="capitalize flex items-center">{group[0]?.shippingaddress?.recipientname} | {group[0]?.shippingaddress?.phone} </div>
                                        <div className="capitalize">{group[0]?.shippingaddress?.address}</div>
                                        <div className="capitalize">{group[0]?.shippingaddress?.district_desc?.toLowerCase()}, {group[0]?.shippingaddress?.city_desc?.toLowerCase()}, {group[0]?.shippingaddress?.province_desc?.toLowerCase()}</div>

                                    </div>
                                    <div className="mt-2 pt-2 border-t">
                                        <div className="font-bold flex items-center gap-4"><span>Metode Pembayaran</span></div>
                                        <div className="flex gap-4 items-start justify-between">
                                            <div className="flex justify-cen items-center gap-4">
                                                <div className="flex gap-4 items-center">
                                                    <img src={group[0]?.paymentmethod_logo} className="w-14 h-auto" />
                                                    {/* <div className="capitalize">{group[0]?.paymentmethod_name}</div> */}
                                                </div>
                                                <div>
                                                    <div>Metode pembayaran menggunakan <span className="font-bold">{group[0]?.paymentmethod_name}</span></div>
                                                </div>
                                            </div>
                                            <div>
                                                {buttonKlik(group[0]?.status, group)}
                                            </div>
                                        </div>
                                    </div>
                                </Panel>
                            ))}
                        </Collapse>
                    ) : (
                        <Zonk />
                    )}
                </>
            )
        },
    ];


    return (
        <BaseLayout>
            <div className="grid grid-cols-3 gap-4">
                <div className="bg-white shadow-lg rounded-lg p-4">
                    <Link href="/transaction/history">
                        <div className="font-bold text-blue-600">Pesanan Saya</div>
                    </Link>
                    <Link href="/user/profile">
                        <div className="font-bold text-slate-600 mt-2">Akun Saya</div>
                    </Link>
                    <InertiaLink
                        href={route('logout')}
                        method="post"
                        as="button"
                    >
                        <div className="flex items-center gap-2 mt-10">
                            <TbLogout className="w-5 h-5 text-red-600" />
                            <div className="text-red-600">Keluar Akun</div>
                        </div>
                    </InertiaLink>
                </div>
                <div className="col-span-2 bg-white shadow-lg rounded-lg p-4">
                    <div className="font-bold capitalize">Pesanan saya</div>
                    <Tabs defaultActiveKey="1" items={items} />
                </div>
            </div>
        </BaseLayout>
    );
}

export default History;


const Zonk = () => {
    return (
        <div className="flex justify-between items-center p-4">
            <img src="/zonk.png" alt="img" />
            <div>
                <div className="text-center font-bold text-lg">Kamu Belum Pernah Berbelanja</div>
                <div className="text-center">Kamu masih belum pernah berbelanja, ayo mulai berbelanja sekarang. Dewo Store memiliki produk menarik lainnya hanya untukmu. Yuk mulia belanja!</div>
                <div className="flex justify-center mt-4">
                    <Link href="/">
                        <PrimaryButton>
                            Mulai Belanja
                        </PrimaryButton>
                    </Link>
                </div>
            </div>
        </div>
    )
}
