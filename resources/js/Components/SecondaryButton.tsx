import classNames from 'classnames';
import React, { PropsWithChildren } from 'react';

type Props = React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
>;

export default function SecondaryButton({
    children,
    ...props
}: PropsWithChildren<Props>) {
    return (
        <button
            {...props}
            className={classNames(
                'bg-white text-sm border py-1.5 px-3 rounded-full border-sky-600 text-sky-600',
                props.className,
            )}
        >
            {children}
        </button>
    );
}
