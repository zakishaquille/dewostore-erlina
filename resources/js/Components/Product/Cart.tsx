import useRoute from "@/Hooks/useRoute";
import { useForm } from "@inertiajs/inertia-react";
import React, { FunctionComponent, useState } from "react";
import { TbBrandWhatsapp, TbShoppingBag, TbSquareRoundedMinus, TbSquareRoundedPlus } from "react-icons/tb";
import PrimaryButton from "../PrimaryButton";
import SecondaryButton from "../SecondaryButton";
import ThirdButton from "../ThirdButton";
import { tsxToIDR } from "../ToIDR";

interface CartsProps {
    stock: number
    amount: number
    slug: string
}

const Carts: FunctionComponent<CartsProps> = (data) => {
    const route = useRoute();
    const [count, setCount] = useState<number>(1);

    const form = useForm({
        quantity: count,
        product_slug: data?.slug,
    });

    function onSubmit(e: React.FormEvent) {
        e.preventDefault();
        form.post(route('cart.store'));
    }
    return (
        <>
            {Number(data?.stock) === 0 ? (
                <div className="shadow-lg rounded-lg p-4 border h-max">
                    <div className="font-bold">Maaf, stok barang sedang berlangsung</div>
                    <div className="text-sm text-slate-600">Silakan menanyakan produk ini kepada customer service kami dengan cara menekan tombol Tanya Produk dibawah ini.</div>
                    <div className="mt-4 flex justify-center">
                        <a href="https://api.whatsapp.com/send?phone=6282132748182&text=Halo CS, Saya melihat tentang produk terkait namun stoknya kosong. Apakah bisa dibantu?&app_absent=0">
                            <ThirdButton className="flex gap-2 items-center justify-center">
                                <TbBrandWhatsapp className="w-4 h-4 text-green-600" />
                                <div>Tanya Whatsapp</div>
                            </ThirdButton>
                        </a>
                    </div>
                </div>
            ) : (
                <>
                    <div className="shadow-lg rounded-lg p-4 border h-max">
                        <div>Ingin beli berapa?</div>
                        <div className="text-xs">Jumlah berapa</div>
                        <div className="flex gap-2 items-center my-2">
                            <button disabled={count === 1} onClick={() => {
                                setCount(count - 1);
                                form.setData("quantity", count - 1)
                            }}>
                                <TbSquareRoundedMinus className="w-5 h-5" />
                            </button>
                            <div>{count}</div>
                            <button disabled={data?.stock <= count} onClick={() => {
                                setCount(count + 1);
                                form.setData("quantity", count + 1)
                            }}>
                                <TbSquareRoundedPlus className="w-5 h-5" />
                            </button>
                            <div>Stok {data?.stock}</div>
                        </div>
                        <hr className="my-2" />
                        <div className="flex justify-between">
                            <div>Subtotal</div>
                            <div>{tsxToIDR(Number(Number(data?.amount) * Number(count)))}</div>
                        </div>
                        <div className="mt-4 flex justify-end">
                            <form onSubmit={onSubmit}>
                                <PrimaryButton type="submit" className="w-max">
                                    <div className="flex items-center gap-2">
                                        <TbShoppingBag className="w-4 h-4" />
                                        <div>Keranjang</div>
                                    </div>
                                </PrimaryButton>
                            </form>
                            {/* <PrimaryButton>Beli sekarang</PrimaryButton> */}
                        </div>
                    </div>
                </>
            )}
        </>
    );
}


export default Carts;
