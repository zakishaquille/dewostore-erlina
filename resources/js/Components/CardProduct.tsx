import useRoute from '@/Hooks/useRoute'
import { Link } from '@inertiajs/inertia-react'
import React from 'react'
import { tsxToIDR } from './ToIDR'

interface Props {
    image: any
    title: any
    subtitle: any
    amount: any
    slug: any
}

const CardProduct = (dt: Props) => {
    const route = useRoute();
    return (
        <div className='bg-slate-100 p-2 w-full h-full rounded-lg'>
            <Link href={route('product.show', dt?.slug)}>
                <div className='bg-slate-200 rounded-lg'>
                    <img src={dt?.image} alt="img" className='rounded-lg' />
                </div>
                <div className='mt-4'>
                    <div className='text-sm text-slate-600 capitalize'>Dewo Store</div>
                    <div className='text-sm font-bold text-slate-600 capitalize mt-1'>{dt?.title}</div>
                    <div className='text-sm text-slate-600 capitalize'>({dt?.subtitle})</div>
                    <div className='text-lg capitalize font-bold text-sky-700'>Rp. {tsxToIDR(dt?.amount)}</div>
                </div> 
            </Link>
        </div>
    )
}

export default CardProduct
